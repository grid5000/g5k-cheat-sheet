DUID=$(shell id -u)
DGID=$(shell id -g)

INAME=g5k-cheat-sheet:latest

TAG=$(shell git tag -l --points-at HEAD~12|tail -1)
DATE=$(shell date +%Y-%m-%d)

help: ## Display available commands in Makefile
	@grep -hE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build_docker_image: ## Build the required image if it does not exist
	@(docker image ls -q $(INAME) | egrep -v '^$$')>/dev/null || docker build --build-arg uid=$(DUID) --build-arg gid=$(DGID) -t $(INAME) .

build_cheat_sheet: build_docker_image ## Build the Grid5000 cheat sheet
	@docker run --rm -it -v $(PWD):/cheat -w /cheat $(INAME) bash -c 'restfully --uri https://public-api.grid5000.fr/stable/ hardware_overview.rb | tee hardware_input'
	@docker run --rm -v $(PWD):/cheat -w /cheat $(INAME) latexmk -pdf -jobname=G5k_cheat_sheet main.tex
