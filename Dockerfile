FROM debian:buster

RUN apt-get update && apt-get install -qqy texlive texlive-latex-extra latexmk ruby-dev build-essential

RUN gem install restfully

ARG uid
ARG gid
RUN groupadd -g ${gid} cheat || true
RUN useradd -u ${uid} -g ${gid} -m cheat -d /cheat || true

USER ${uid}
