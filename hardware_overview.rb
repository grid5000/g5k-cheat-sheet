# (c) 2012-2016 Inria by David Margery (david.margery@inria.fr) in the context of the Grid'5000 project
# Licenced under the CeCILL B licence.

def get_ib_type(rate)
  case rate / 1e9
  when 10
    " SDR"
  when 20
    " DDR"
  when 40
    " QDR"
  when 56
     " FDR"
  when 100
    " EDR"
  else
    ""
  end
end

root.sites.each do |site|
  begin
    puts "\\textbf{#{site["uid"].capitalize}} &  &  &  &  &  &  &  &  \\\\"
    site.clusters.each do |cluster|
      date = cluster["created_at"].split(" ")[3]
      cluster_size = cluster.nodes.length
      node = cluster.nodes[0]
      arch = node["architecture"]
      nb_cpu = arch["nb_procs"]
      nb_core = arch["nb_cores"]/nb_cpu
      cpu_type = arch["platform_type"]
      cpu_speed = node["processor"]["clock_speed"].to_i/(10E8)
      memory = (node["main_memory"]["ram_size"].to_i.to_f / (1024**3).to_f).round
      storages = {}
      node['storage_devices'].each do |device|
        unless device['storage'].nil?
          storages[device['storage']].nil? ? storages[device['storage']] = [(device['size'].to_i / 1024**3).round] : storages[device['storage']] += [(device['size'].to_i / 1024**3).round]
        end
      end
      cluster_storages = ''
        storages.each do |t, s|
          sizes = Hash.new(0)
          s.each { |v| sizes.store(v, sizes[v]+1) }
          sizes.each do |size, nb|
            if nb > 1
              cluster_storages += " #{nb}x#{size}GB #{t},"
            else
              cluster_storages += " #{size}GB #{t},"
            end
          end # each size
        end # each storage
      cluster_storages = cluster_storages[0..-2]
      int_count = {}
      node['network_adapters'].select { |n| n['mountable'] }
        .each do |d|
        intf = d['interface']
        key = (d['rate'] / 1e9).to_i.to_s + 'Gb ' + intf +
          (intf == 'InfiniBand' ? get_ib_type(d['rate']) : '')
        if int_count.key?(key)
          int_count[key] += 1
        else
          int_count[key] = 1
        end
      end
      int_desc = int_count.map { |k, v| "#{v} x #{k}" }.join(', ')
      gpu = ""
      if node['gpu_devices']
        gpu_model = node['gpu_devices'].first[1]['model']
        gpu_count = node['gpu_devices'].length
        gpu = "#{gpu_count} x #{gpu_model}"
      end
      puts "#{cluster["uid"]} & (#{date}) & #{cluster_size} & #{nb_cpu}x#{nb_core}cores & @#{cpu_speed}Ghz & #{memory}GB & #{cluster_storages} & #{gpu} & #{int_desc} \\\\"
    end
  rescue Restfully::HTTP::ServerError => e
    puts "Could not access information from #{site["uid"]}"
    puts e.backtrace
  rescue StandardError => e
    puts e
    puts e.backtrace
    exit
  end
end
