# g5k-cheat-sheet
Grid'5000 Cheat Sheet for usefull commands

## Build recipe
### Requirement
- ruby <= 2.1
- restfully 1.2.0
- latex packages

### Hardware list update
Run `restfully --uri https://api.grid5000.fr/stable/ hardware_overview.rb` and
copy/paste the output in hardware_overview.tex

### Latex generation
`latexmk -pdf -jobname=G5k_cheat_sheet main.tex` which will generate `G5k_cheat_sheet.pdf`

### Update on mediawiki
[Upload a new file here](https://www.grid5000.fr/w?title=Special:Upload&wpDestFile=G5k_cheat_sheet.pdf&wpForReUpload=1)

## Using the Dockerfile

Convenient if you do not have the required versions of Ruby or some gems and Latex at your quick disposal.

### Build the required docker image and build the cheat sheet
```
% make build_cheat_sheet
```

### Check the generated PDF
```
% xdg-open G5k_cheat_sheet.pdf
```

### Update on mediawiki
[Upload a new file here](https://www.grid5000.fr/w?title=Special:Upload&wpDestFile=G5k_cheat_sheet.pdf&wpForReUpload=1)

### Git push the updates
```
% git add .
% git commit -m"Build the Grid5000 Cheat Sheet for the brand new cluster Groovy"
% git push
```

